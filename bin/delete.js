const fs = require('fs');
const globSync = require('./glob-sync');
const nodePath = require('path');
const tsConfig = require('../tsconfig.json');

const outDir = tsConfig.compilerOptions.outDir;
const rootDir = tsConfig.compilerOptions.rootDir;

module.exports = (extension, options = {}) => {
  if (!extension || extension && extension.length === 0) {
    console.log('No file extension provided');
    return process.exit(1);
  }

  const isWildCard = extension === '*' ? true : false;
  const isJson = extension === 'json' ? true : false;

  const path = nodePath
    .resolve(__dirname, '..', isWildCard || isJson ? outDir : rootDir, '**', `*.${extension}`);

  globSync(path, options)
    .then(files => {
      const destinationFiles = [];
      if (files && files.length > 0) {
        files.map(file => {
          const relativePath = nodePath
            .relative(nodePath.resolve(__dirname, '..', rootDir), file);
  
          let destination = isWildCard || isJson ? file : nodePath
            .resolve(__dirname, '..', outDir, relativePath);

          if (!(isWildCard || isJson) && extension !== 'js') {
            destination = destination.replace(`.${extension}`, '.js');
          }

          destinationFiles.push(destination);
          if (!isWildCard || !isJson) {
            const map = destination + '.map';
            destinationFiles.push(map);
          }
        });
      }      
      return destinationFiles;
    })
    .then(files => {
      const deletePromises = [];
      if (files && files.length > 0) {
        files.map(
          file => fs.existsSync(file) && deletePromises.push(
            fs.unlinkSync(file)
        ));
      }
      return Promise.all(deletePromises);
    })
    .then(() => {
      process.exit();
    })
    .catch(err => {
      console.log(err);
      process.exit(1);
    });
};
