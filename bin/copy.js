const fs = require('fs');
const globSync = require('./glob-sync');
const nodePath = require('path');
const tsConfig = require('../tsconfig.json');

const rootDir = tsConfig.compilerOptions.rootDir;
const outDir = tsConfig.compilerOptions.outDir;

module.exports = (extension, options = {}) => {
  if (!extension || extension && extension.length === 0) {
    console.log('No file extension provided');
    return process.exit(1);
  }

  const path = nodePath
    .resolve(__dirname, '..', rootDir, '**', `*.${extension}`);

  globSync(path, options)
    .then(files => {
      const copyPromises = [];
      if (files && files.length > 0) {
        files.map(file => {
          const relativePath = nodePath
            .relative(nodePath.resolve(__dirname, '..', rootDir), file);

          const destination = nodePath
            .resolve(__dirname, '..', outDir, relativePath);

          copyPromises.push(fs.copyFileSync(file, destination));
        });
      }
      return Promise.all(copyPromises);
    })
    .then(() => {
      process.exit();
    })
    .catch(err => {
      console.log(err);
      process.exit(1);
    });
};
