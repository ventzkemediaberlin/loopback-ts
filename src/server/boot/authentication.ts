
import { LoopBackApplication } from "loopback";

export const authentication = (app: LoopBackApplication) => {
  app.enableAuth();
};

export default authentication;