import { Router } from 'express'
import { LoopBackApplication, } from 'loopback';

interface RootProps extends LoopBackApplication {
  loopback: {
    Router(): Router;
    status: any;
  }
}

export const root = (app: RootProps) => {
  const router = app.loopback.Router();
  router.get('/', app.loopback.status());
  app.use(router);
}

export default root;
